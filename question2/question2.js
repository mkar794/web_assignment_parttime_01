"use strict";


/**
 * This function will create a single table and populate it with the information contained in the char_freq_map map.
 * 
 * The table will have the given id and class name, and will be sorted according to the ordering of the given
 * sortedKeys array.
 */
function createTable(table, char_freq_map, sortedKeys) {
    
    // TODO step 3: implement this function. Details are in the assignment handout.
    console.log("this is looping how many times ??");


    //var table = document.getElementById("table-alphasort");
    var tbody = document.createElement("tbody");
    table.appendChild(tbody);

    var row;
    for( var i = 0; i < sortedKeys.length; i++ ) {
        row = $("<tr><td>" + sortedKeys[i]   + "</td><td>" + char_freq_map[sortedKeys[i]] + "</td></tr>");
        $(tbody).append(row);
        console.log("this is looping 2 times ??");
    }

}


/**
 * This function will loop through all the characters in the supplied text, and use a JavaScript
 * object to store info about the number of each letter that appears in the string. The info will be
 * stored in the char_freq_map.
 * 
 * Once that's done, we'll create two tables to display the info to the user. One will be sorted by
 * alphabet, the other will be sorted by frequency in descending order.
 */
function process(text) {

    // This object will store the data - it will map each character from A - Z, with the number of times
    // that character appears. For example, char_freq_map["a"] will give us the number of times "a"
    // or "A" appears in the text (we're ignoring case).
    var char_freq_map = {};

    var i = -1;
    var character = "";
    var keysArray; // an array of the keys
    var contents = "";

    // Populate char_freq_map with character frequency information
    for (i = 0; i < text.length; i++) {
        character = text.charAt(i);

               // Skip punctuation and numbers to deal with just the chars from the alphabet
        if ((character >= 'a' && character <= 'z') || (character >= 'A' && character <= 'Z')) {
            character = character.toLowerCase(); // Ignore case

            // TODO step 2: if char_freq_map already contains an entry for the given character,
            // add one to that entry. Otherwise, add a new entry for that character with the
            // initial value of 1.


            if(char_freq_map[character] === undefined) {
               char_freq_map[character] = 1;

            } else {
               char_freq_map[character] += 1;

            }
        }
    }


    // Obtain arrays of all characters in char_freq_map, sorted both alphabetically (alphaSortedKeys)
    // and by frequency (freqSortedKeys).

    var alphaSortedKeys = Object.keys(char_freq_map).sort();

    console.log("alphaSortedKeys: " + alphaSortedKeys);
    //console.log("char_freq_map11 : " + JSON.stringify(char_freq_map));


    var freqSortedKeys = Object.keys(char_freq_map).sort(function (a, b) {
        return char_freq_map[b] - char_freq_map[a];
    });
    console.log("freqsortedkeys: " + freqSortedKeys);


    // TODO step 4: Call the createTable function twice, to populate the tables.
    createTable(document.getElementById("table-alphasort"), char_freq_map, alphaSortedKeys);
    createTable(document.getElementById("table-freqsort"), char_freq_map, freqSortedKeys);
}




/**
 * When the document is ready, we'll add an event handler to the button which will initiate processing.
 */
$(document).ready(function () {

    // TODO step 1: add an event handler to the analyze button, so that when the button is clicked, the process() function
    // will be called, with the text area's text passed in as an argument.
   

    var analyze = document.getElementById("analyzebutton");
    
    analyze.onclick = function() {
        var text = document.getElementById("thetext").value;
        process(text);
        console.log("test: " + text);
    }

    // $("#analyzebutton").click = function() {
       
    //     process(text);
    //     console.log("test");
    // }

});