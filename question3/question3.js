"use strict";

// Created a template which will be used for inserting new article HTML.
var articleTemplate =
    '<div class="card mt-4">' +
    '<h5 class="card-header">' +
    'Title goes here.' +
    '</h5>' +
    '<div class="card-body">' +
    '<p class="card-text">Article text goes here.</p>' +
    '<button type="button" class="btn btn-primary">Show full text</button>' +
    '</div>' +
    '</div>';


// Method for ajax call when button is clicked
    // function getTextContents(articles) {
    //     $.ajax({
    //         url: 'https://sporadic.nz/ajax/articles',
    //         type: 'GET',
    //         data: articles.id,

    //         success: function(articles) {
    //             var p = $("<p></p>");
    //             p.addClass("card-text");
    //             p.text("article content" + articles);
    //             console.log("article content after button Click: " + articles);
    //             console.log("I should get article paragraph");
                
    //         }
    //     });
    // }
/**
 * Adds the given article (represented by a JSON object) to the given container.
 * articleTemplate will be used as the template for displaying the articles.
 */

function addArticle(container, article) {

    //create main div for article 
    var card = $("<div></div>");
    console.log("does it reach here") //comment checks
    card.addClass("card mt-4");
    //card.addClass("");

    //create header for article  
    var cardHeader = $("<h5></h5>");
    cardHeader.addClass("card-header");
    cardHeader.text(article.title);
    card.append(cardHeader);

    //create div for article body 
    var cardBody = $("<div></div>");
    cardBody.addClass("card-body");
    //create paragraph for article body 
    var p = $("<p></p>");
    p.addClass("card-text");
    p.text(article.content);

    //create button and append
    var btn = $("<button></button>");
    btn.attr("type", "button");
    btn.addClass("btn btn-primary");
    btn.text("Show full text");

    //click function for button for AJAX CALL
    btn.click(function() {

        var theButton = $(this);

        $.ajax({
            url: 'https://sporadic.nz/ajax/articles',
            type: 'GET',
            data:{id:article.id},

            success: function(articles) {

                var p = $(btn).prev(); //This method gets the immediate preceding sibling of its element
                p.text(articles.content);
                console.log("article content after button Click: " + articles.content);
                //console.log("I should get article paragraph");
                theButton.remove();
            }
        });
    });

    cardBody.append(p);
    cardBody.append(btn);

    card.append(cardBody);
    //append all divs in main container
    container.append(card);

    //CONSOLE LOG TO CHECK VALUES

    console.log("id: " + article.id + ", title: " +
        article.title + ", content: " + article.content);

    // TODO Complete this.
}

/**
 * When page loads, we will execute an AJAX request for all articles.
 * When the request is complete, we will loop through the returned JSON array, and for each element in that
 * array, we will create a new div from the template, and populate it with data obtained from the AJAX request.
 */
$(document).ready(function () {

    $.ajax({
        url: 'https://sporadic.nz/ajax/articles',
        type: 'GET',
        success: function (articles) {

            var container = $("#article-container");
            container.empty();

            for (var i = 0; i < articles.length; i++) {

                var article = articles[i];

                addArticle(container, article);
            }
        }
    });
});