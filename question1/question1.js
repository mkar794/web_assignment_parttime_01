/* Develop your answer to Question 1 here */
"use strict";

var current_banner_num = 1;
var intervalTimer = null;
var speed = 2500;

function cycleImages() {
	$("#banner-" + current_banner_num).fadeOut( 3000);
	if (current_banner_num == 4) {
		current_banner_num = 1;
		
	}
	else {
		current_banner_num++;
	}
	$("#banner-" + current_banner_num).fadeIn( 3000);
}

$(document).ready(function () {

	$("#banner-base").css("visibility", "hidden"); // Use hidden to keep width and height of div same as image
	$("#banner-" + current_banner_num).show();

	var buttonsDiv = $("#buttons");
	var start_button = $("<button>Start</button>");
	buttonsDiv.append(start_button);
	var stop_button = $("<button>Stop</button>");
	buttonsDiv.append(stop_button);

	// Create Range
	var divRange = $("<div></div>");
	divRange.addClass ("slidecontainer");

	var inputRange = $("<input></input>");
	inputRange.attr("id", "range");
	inputRange.attr("type", "range" );
	inputRange.attr("min", 1000);
	inputRange.attr("max", 5000);
	inputRange.attr("value", 2500);
	divRange.append(inputRange);
	buttonsDiv.append(divRange);

	var slider = document.getElementById("range");
	slider.oninput = function() {

		speed = inputRange.val();
		
	}


	// When the "start" button is clicked, cycle to a new banner image.
	
	$(start_button).click(function () {
		
		console.log("speed = " + speed);
	

		// Show the current banner
		//$("#banner-" + current_banner_num).show();
		

		if (intervalTimer == null) {
			intervalTimer = setInterval("cycleImages()", speed);
		}
		
	});


	$(stop_button).click(function() {
		console.log("stop button ");
		//$("#banner-" + current_banner_num).stop(true);
		if(intervalTimer != null){
			
			clearInterval(intervalTimer);
			intervalTimer = null;
		 	
		 }


	});


});

